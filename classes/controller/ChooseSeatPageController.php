<?php

require_once(__DIR__.'/../model/Flight.php');
require_once(__DIR__.'/../model/User.php');
require_once(__DIR__.'/../model/Seat.php');
require_once(__DIR__.'/PageController.php');

class ChooseSeatPageController extends PageController {
    private $user;
    private $flight;

    public function __construct() {
        parent::__construct();
        $this->flight = new Flight();
        $this->flight->getFromSession();
        $this->user = new User();
        $this->user->getFromSession();
    }

    public function addSeatToFlight() {
        $this->flight->removeSeats();

        $seat_array = get($_POST, 'seats', array());
        $child_array = get($_POST, 'child', array());
        $wheelchair_array = get($_POST, 'wheelchair', array());
        $special_diet_array = get($_POST, 'special_diet', array());

        if (!empty($seat_array)) {
            foreach ($seat_array as $seat_no) {
                $seat = new Seat($seat_no);

                if (in_array($seat_no, $child_array)) {
                    $seat->setChild(true);
                }
                if (in_array($seat_no, $wheelchair_array)) {
                    $seat->setWheelchair(true);
                }
                if (in_array($seat_no, $special_diet_array)) {
                    $seat->setSpecialDiet(true);
                }

                $this->flight->addSeat($seat);
            }
            $flight_id = $this->user->addFlight($this->flight);
            $this->flight->setId($flight_id);
            $this->flight->pushToSession();
            $this->user->pushToSession();
            return true;
        }
        return false;
    }

    public function submit() {
        if ($this->addSeatToFlight()) {
            header(sprintf('Location: %sindex.php?page=currently-booked', Constants::BASE_URL));
        } else {
            $this->error->addWarning('Please select a seat.');
            $this->error->pushToSession();
            header(sprintf('Location: %sindex.php?page=choose-seat', Constants::BASE_URL));
        }
    }
}
