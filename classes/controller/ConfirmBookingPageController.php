<?php

require_once(__DIR__.'/../model/Flight.php');
require_once(__DIR__.'/../model/User.php');
require_once(__DIR__.'/PageController.php');

class ConfirmBookingPageController extends PageController {
    private $user;
    private $flight;

    public function __construct() {
        parent::__construct();
        $this->flight = new Flight();
        $this->flight->getFromSession();
        $this->user = new User();
        $this->user->getFromSession();
    }

    public function getBookedSeatTable() {
        $html_string = '';
        $seats = $this->flight->getSeats();
        foreach ($seats as $seat) {
            $html_string .= '<tr>';
            $html_string .= sprintf('<td>%s</td>', $seat->getSeatNo());

            if ($seat->getChild()) {
                $html_string .= '<td style="background-color: #dff0d8;"></td>';
            } else {
                $html_string .= '<td></td>';
            }

            if ($seat->getWheelchair()) {
                $html_string .= '<td style="background-color: #dff0d8;"></td>';
            } else {
                $html_string .= '<td></td>';
            }

            if ($seat->getSpecialDiet()) {
                $html_string .= '<td style="background-color: #dff0d8;"></td>';
            } else {
                $html_string .= '<td></td>';
            }

            $html_string .= '</tr>';
        }
        return $html_string;
    }

    public function createMessage() {
        $given_name = $this->user->getGivenName();
        $family_name = $this->user->getFamilyName();
        $from_city = $this->flight->getFromCity();
        $to_city = $this->flight->getToCity();
        $price = $this->flight->getPrice();
        $address = sprintf('%s  %s  %s  %s  %s', $this->user->getAddress1(), $this->user->getSuburb(), $this->user->getState(), $this->user->getCountry(), $this->user->getPostcode());

        $message = sprintf('
            <table width="100%%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <table width="100%%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td align="left">Hi %s %s,</td>
                                <td align="right">%s</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center">Flight from %s to %s</td>
                </tr>
                <tr>
                    <td align="center">Price: $%s</td>
                </tr>
                <tr>
                    <td align="center">Seat Table<td>
                </tr>
                <tr>
                    <td>
                        <table width="100%%" border="1px solid">
                            <thead>
                                <tr>
                                    <th>Seat No</th>
                                    <th>Child</th>
                                    <th>Wheelchair</th>
                                    <th>Special Diet</th>
                                </tr>
                            </thead>
                            <tbody>
                                %s
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>
            ', 
            $given_name,
            $family_name,
            $address,
            $to_city,
            $from_city,
            $price,
            $this->getBookedSeatTable()
        );
        return $message;
    }

    public function mailInfo() {
        $to = $this->user->getEmailAddress();

        $from_city = $this->flight->getFromCity();
        $to_city = $this->flight->getToCity();
        $subject = sprintf('Flight from %s to %s', $from_city, $to_city);

        $message = $this->createMessage();

        $headers = 'From: info@flibooking.com.au'."\r\n";
        $headers .= 'MIME-Version: 1.0'."\r\n";
        $headers .= 'Content-type:text/html;charset=UTF-8'."\r\n";

        mail($to, $subject, $message, $headers);
    }

    public function submit() {
        $this->mailInfo();
        header(sprintf('Location: %sindex.php?page=done', Constants::BASE_URL));
    }
}
