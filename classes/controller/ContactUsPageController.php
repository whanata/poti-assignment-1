<?php

require_once(__DIR__.'/PageController.php');

class ContactUsPageController extends PageController {

    public function __construct() {
        parent::__construct();
        $this->fields = array(
            'first_name' => '',
            'last_name' => '',
            'email_address' => '',
            'email_subject' => '',
            'content' => '',
        );

        $this->getFields();
    }

    public function getFields() {
        foreach (array_keys($this->fields) as $field_key) {
            $this->fields[$field_key] = get($_POST, $field_key);
        }
    }

    public function checkRequiredField() {
        $required_fields = array(
            'email_subject',
            'email_address',
            'content'
        );

        foreach ($required_fields as $field_key) {
            if (empty($this->fields[$field_key])) {
                return false;
            }
        }

        return true;
    }

    public function validateFields() {
        foreach ($this->fields as $key => $value) {
            if (empty($value)) {
                continue;
            }

            if ($key === 'email_address') {
                if (!Validator::validateEmail($value)) {
                    $this->error->addWarning('Email must be in email format');
                }
            }
        }
    }

    public function sendEmail() {
        $to = '11993577@student.uts.edu.au';

        $subject = $this->fields['email_subject'];

        $message = $this->fields['content'];

        $headers = sprintf('From: %s', $this->fields['email_address'])."\r\n";

        mail($to, $subject, $message, $headers);
    }

    public function confirmationEmail() {
        $to = $this->fields['email_address'];

        $subject = 'Confirmation email to FliBooking';

        $message = 'We have received this message and will contact you within 1 - 3 Business Days.';

        $headers = 'From: info@flibooking.com.au'."\r\n";

        mail($to, $subject, $message, $headers);
    }

    public function submit() {
        if ($this->checkRequiredField() === false) {
            $this->error->addWarning('One or more compulsory fields are blank');
        }
        $this->validateFields();
        $this->error->pushToSession();
        if ($this->error->isNone()) {
            $this->sendEmail();
            $this->confirmationEmail();
            header(sprintf('Location: %sindex.php', Constants::BASE_URL));
        } else {
            $session = new Session();
            $session->setSession('fields', $this->fields);
            header(sprintf('Location: %sindex.php?page=contact-us', Constants::BASE_URL));
        }
    }
}
