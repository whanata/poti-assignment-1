<?php

require_once(__DIR__.'/../model/Flight.php');
require_once(__DIR__.'/../model/User.php');
require_once(__DIR__.'/../model/Seat.php');
require_once(__DIR__.'/PageController.php');

class CurrentlyBookedPageController extends PageController {
    private $flight;
    private $user;

    public function __construct() {
        parent::__construct();
        $this->flight = new Flight();

        if ($this->flight->getFromSession() === false) {
            render404();
        }

        $this->user = new User();
        if ($this->user->getFromSession() === false) {
            render404();
        }
    }

    public function clearFlight() {
        $this->user->deleteFlights();
        $this->user->pushToSession();
        header(sprintf('Location: %sindex.php', Constants::BASE_URL));
    }

    public function submit() {
        header(sprintf('Location: %sindex.php?page=personal-detail', Constants::BASE_URL));
    }
}
