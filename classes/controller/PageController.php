<?php

require_once(__DIR__.'/../../util/util.php');

class PageController {
    protected $error;
    protected $session;

    public function __construct() {
        $this->error = new Error();
        $this->session = new Session();
    }
}
