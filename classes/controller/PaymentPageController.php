<?php

require_once(__DIR__.'/../model/Flight.php');
require_once(__DIR__.'/../model/User.php');
require_once(__DIR__.'/../model/CreditCard.php');
require_once(__DIR__.'/PageController.php');

class PaymentPageController extends PageController {
    private $user;
    private $flight;
    private $fields;

    public function __construct() {
        parent::__construct();
        $this->flight = new Flight();
        $this->flight->getFromSession();
        $this->user = new User();
        $this->user->getFromSession();

        $this->fields = array(
            'credit_card_type' => '',
            'credit_card_number' => '',
            'credit_card_name' => '',
            'expiry_month' => '',
            'expiry_year' => '',
            'security_code' => '',
        );

        $this->getFields();
    }

    public function getFields() {
        foreach (array_keys($this->fields) as $field_key) {
            $this->fields[$field_key] = get($_POST, $field_key);
        }
    }

    public function checkRequiredField() {
        foreach ($this->fields as $key => $value) {
            if (empty($value)) {
                return false;
            }
        }

        return true;

    }

    public function validateFields() {
        foreach ($this->fields as $key => $value) {
            if (empty($value)) {
                continue;
            }

            if ($key === 'credit_card_number') {
                if (!Validator::validate('credit_card_number', $value)) {
                    $this->error->addWarning('Credit Card Number can only have 12 digits');
                }
            }

            if ($key === 'security_code') {
                if (!Validator::validate('ccv', $value)) {
                    $this->error->addWarning('Security Code can only have 3 digits');
                }
            }

            if ($key === 'expiry_year') {
                $current_year = date('y');
                $current_month = date('m');
                if ($value == $current_year && $this->fields['expiry_month'] <= $current_month) {
                    $this->error->addWarning('Credit Card already expired - Please check the Month and Year Expired');
                }
            }

        }

    }

    public function addCreditCardToUser() {
        $credit_card = new CreditCard();
        $credit_card->setType($this->fields['credit_card_type']);
        $credit_card->setNumber($this->fields['credit_card_number']);
        $credit_card->setName($this->fields['credit_card_name']);
        $credit_card->setExpiryMonth($this->fields['expiry_month']);
        $credit_card->setExpiryYear($this->fields['expiry_year']);
        $credit_card->setSecurityCode($this->fields['security_code']);
        $this->user->setCreditCard($credit_card);
        $this->user->pushToSession();
    }

    public function submit() {
        if ($this->checkRequiredField() === false) {
            $this->error->addWarning('One or more compulsory fields are blank');
        }
        $this->validateFields();
        $this->error->pushToSession();

        if ($this->error->isNone()) {
            $this->addCreditCardToUser();
            header(sprintf('Location: %sindex.php?page=confirm-booking', Constants::BASE_URL));
        } else {
            header(sprintf('Location: %sindex.php?page=payment', Constants::BASE_URL));
        }
    }
}
