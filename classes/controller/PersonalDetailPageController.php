<?php

require_once(__DIR__.'/../model/Flight.php');
require_once(__DIR__.'/../model/User.php');
require_once(__DIR__.'/PageController.php');

class PersonalDetailPageController extends PageController {
    private $user;
    private $flight;
    private $fields;

    public function __construct() {
        parent::__construct();
        $this->flight = new Flight();
        $this->flight->getFromSession();
        $this->user = new User();
        $this->user->getFromSession();

        $this->fields = array(
            'given_name' => '',
            'family_name' => '',
            'address1' => '',
            'address2' => '',
            'suburb' => '',
            'postcode' => '',
            'state' => '',
            'country' => '',
            'email' => '',
            'mobile_phone' => '',
            'business_phone' => '',
            'work_phone' => ''
        );

        $this->getFields();
    }

    public function getFields() {
        foreach (array_keys($this->fields) as $field_key) {
            $this->fields[$field_key] = get($_POST, $field_key);
        }
    }

    public function checkRequiredField() {
        $required_fields = array(
            'given_name',
            'family_name',
            'address1',
            'suburb',
            'country',
            'email'
        );

        $extra_fields = array(
            'state',
            'postcode'
        );

        foreach ($required_fields as $field_key) {
            if (empty($this->fields[$field_key])) {
                return false;
            }
        }

        if ($this->fields['country'] === 'Australia') {
            foreach ($extra_fields as $field_key) {
                if (empty($this->fields[$field_key])) {
                    return false;
                }
            }
        }

        return true;
    }

    public function validateFields() {
        $phone_fields = array('mobile_phone', 'business_phone', 'work_phone');

        foreach ($this->fields as $key => $value) {
            if (empty($value)) {
                continue;
            }
            if ($key === 'postcode') {
                if (!Validator::validate('postcode', $value)) {
                    $this->error->addWarning('Postcode must be alphanumeric');
                }
            }
            if ($key === 'email') {
                if (!Validator::validateEmail($value)) {
                    $this->error->addWarning('Email must be in email format');
                }
            }

            if (in_array($key, $phone_fields)) {
                if (!Validator::validate('phone', $value)) {
                    $this->error->addWarning(str_replace('_', ' ', ucfirst($key)).' must be a valid phone number');
                }
            }
        }
    }

    public function addUserDetail() {
        $this->user->setGivenName($this->fields['given_name']);
        $this->user->setFamilyName($this->fields['family_name']);
        $this->user->setEmailAddress($this->fields['email']);
        $this->user->setAddress1($this->fields['address1']);
        $this->user->setAddress2($this->fields['address2']);
        $this->user->setSuburb($this->fields['suburb']);
        $this->user->setState($this->fields['state']);
        $this->user->setCountry($this->fields['country']);
        $this->user->setPostcode($this->fields['postcode']);
        $this->user->setMobilePhone($this->fields['mobile_phone']);
        $this->user->setBusinessPhone($this->fields['business_phone']);
        $this->user->setWorkPhone($this->fields['work_phone']);
        $this->user->pushToSession();
    }

    public function submit() {
        if ($this->checkRequiredField() === false) {
            $this->error->addWarning('One or more compulsory fields are blank');
        }
        $this->validateFields();
        $this->error->pushToSession();
        if ($this->error->isNone()) {
            $this->addUserDetail();
            header(sprintf('Location: %sindex.php?page=payment', Constants::BASE_URL));
        } else {
            $session = new Session();
            $session->setSession('fields', $this->fields);
            header(sprintf('Location: %sindex.php?page=personal-detail', Constants::BASE_URL));
        }
    }
}
