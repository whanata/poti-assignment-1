<?php

require_once(__DIR__.'/../model/Flight.php');
require_once(__DIR__.'/PageController.php');
require_once(__DIR__.'/../../util/util.php');

class SearchFlightPageController extends PageController {
    private $flight;

    public function __construct() {
        parent::__construct();
        $this->flight = new Flight();
    }

    public function getSelectedFlight() {
        if (in_array('selected_flight', array_keys($_POST)) && !empty($_POST['selected_flight'])) {
            $this->flight->setRouteNo($_POST['selected_flight']);
            $this->flight->load();
            $this->flight->pushToSession();
            return true;
        } else {
            return false;
        }
    }

    public function submit() {
        if ($this->getSelectedFlight()) {
            header(sprintf('Location: %sindex.php?page=choose-seat', Constants::BASE_URL));
        } else {
            $this->error->addWarning('Please select a flight.');
            $this->error->pushToSession();
            header(sprintf('Location: %sindex.php?page=search-flight', Constants::BASE_URL));
        }
    }


}
