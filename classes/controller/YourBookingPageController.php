<?php

require_once(__DIR__.'/../model/User.php');
require_once(__DIR__.'/../model/Flight.php');
require_once(__DIR__.'/PageController.php');

class YourBookingPageController extends PageController {
    private $user;

    public function __construct() {
        parent::__construct();
        $this->user = new User();
        $this->user->getFromSession();
    }

    public function deleteFlights() {
        $selected_flight = get($_POST, 'selected_flight');
        if (!empty($selected_flight)) {
            $this->user->deleteSelectedFlights($selected_flight);
        } else {
            $this->error->addWarning('No flights selected');
        }
    }

    public function checkout() {
        $selected_flight = get($_POST, 'selected_flight');
        if (!empty($selected_flight)) {
            if (count($selected_flight) === 1) {
                $flight = new Flight();
                $flight->copy($this->user->getFlight($selected_flight[0]));
                $flight->pushToSession();
                return true;
            } else {
                $this->error->addWarning('Only 1 flight can be checked out');
                return false;
            }
        } else {
            $this->error->addWarning('No flights selected');
            return false;
        }
    }

    public function submit() {
        $checkout = false;
        if (array_key_exists('delete_flight', $_POST)) {
            $this->deleteFlights();
        } elseif (array_key_exists('checkout', $_POST)) {
            $checkout = $this->checkout();
        }
        $this->error->pushToSession();
        $this->user->pushToSession();
        if ($checkout) {
            header(sprintf('Location: %sindex.php?page=personal-detail', Constants::BASE_URL));
        } else {
            header(sprintf('Location: %sindex.php?page=your-booking', Constants::BASE_URL));
        }
    }
}
