<?php

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

class DB {

    public static function connect($host, $user, $password, $database) {
        try {
            $conn = new mysqli($host, $user, $password, $database);
            return $conn;
        } catch (mysqli_sql_exception $e) {
            error_log($e->errorMessage());
            return false;
        }
    }

    // Binds = array('<Type of Varaible (sidb)>', <variabl1>, <variable2>, ...)
    // s = String, i = Integer, d = Double, b = Boolean
    public static function query($sql) {
        $conn = self::connect('rerun', 'potiro', 'pcXZb(kL', 'poti');
        $esc_sql = $conn->escape_string($sql);
        
        try {
            $db_result = $conn->query($sql);

            $conn->close();
            return $db_result;
        } catch (mysqli_sql_exception $e) {
            error_log($e->errorMessage());
            $conn->close();
            return false;
        }
    }

    public static function getResult($db_result) {
        $result = array();
        if ($db_result instanceof mysqli_result) {
            while($row = $db_result->fetch_assoc()) {
                array_push($result, $row);
            }
            $db_result->free();
            return $result;
        }
        return false;
    }
}
