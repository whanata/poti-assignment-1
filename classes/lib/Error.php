<?php

require_once(__DIR__.'/SessionObject.php');

class Error extends SessionObject implements SessionObjectInterface {
    const SESSION_KEY = 'error';

    private $errorMessages;
    private $warningMessages;

    public function __construct() {
        parent::__construct();
        $this->errorMessages = array();
        $this->warningMessages = array();
    }

    public function copy($object) {
        $this->errorMessages = $object->getErrorMessages();
        $this->warningMessages = $object->getWarningMessages();
    }

    public function addError($message) {
        array_push($this->errorMessages, $message);
    }

    public function addWarning($message) {
        array_push($this->warningMessages, $message);
    }

    public function getErrorMessages() {
        return $this->errorMessages;
    }

    public function getErrorHtml() {
        $error_html = '';
        if (!empty($this->errorMessages)) {
            foreach ($this->errorMessages as $error) {
                $error_html .= '<div class="alert alert-error">';
                $error_html .= $error;
                $error_html .= '</div>';
            }
        }
        return $error_html;
    }

    public function getWarningMessages() {
        return $this->warningMessages;
    }

    public function getWarningHtml() {
        $warning_html = '';
        if (!empty($this->warningMessages)) {
            foreach ($this->warningMessages as $warning) {
                $warning_html .= '<div class="alert alert-warning">';
                $warning_html .= $warning;
                $warning_html .= '</div>';
            }
        }
        return $warning_html;
    }

    public function isNone() {
        if (empty($this->errorMessages) && empty($this->warningMessages)) {
            return true;
        }
        return false;
    }
}
