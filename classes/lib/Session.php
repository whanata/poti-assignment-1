<?php

require_once(__DIR__.'/../../util/util.php');

class Session {

    public function __construct() {
        session_start();
    }

    public function setSession($key, $value) {
        $_SESSION[$key] = $value;
    }

    public function getSession($key, $unset, $replacement = '') {
        $value = get($_SESSION, $key, $replacement);
        if ($unset) {
            unset($_SESSION[$key]);
        }
        return $value;
    }
}
