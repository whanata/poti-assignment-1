<?php

require_once(__DIR__.'/Session.php');
require_once(__DIR__.'/SessionObjectInterface.php');

class SessionObject {
    protected $session;

    public function __construct() {
        $this->session = new Session();
    }

    public function pushToSession() {
        $this->session->setSession(static::SESSION_KEY, serialize($this));
    }

    public function getFromSession($unset = false) {
        $object = unserialize($this->session->getSession(static::SESSION_KEY, $unset));
        if (!empty($object)) {
            $this->copy($object);
            return true;
        }
        return false;
    }
}
