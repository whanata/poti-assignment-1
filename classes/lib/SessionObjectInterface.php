<?php

interface SessionObjectInterface {

    public function copy($object);
}
