<?php

require_once(__DIR__.'/../../util/util.php');

class Validator {

    public static function validate($type, $value) {
        $valid_regex = array(
            'postcode' => '/^[0-9A-Za-z]+$/i',
            'phone' => '/^[0-9]+$/i',
            'credit_card_number' => '/^[0-9]{12}$/i',
            'ccv' => '/^[0-9]{3}$/i',
        );

        return preg_match($valid_regex[$type], $value);
    }

    public static function validateEmail($value) {
        if (filter_var($value, FILTER_VALIDATE_EMAIL)) {
            return true;
        } else {
            return false;
        }
    }
}
