<?php

class CreditCard {
    private $type;
    private $number;
    private $name;
    private $expiryMonth;
    private $expiryYear;
    private $securityCode;

    public function __construct() {
        $this->type = null;
        $this->number = null;
        $this->name = null;
        $this->expiryMonth = null;
        $this->expiryYear = null;
        $this->securityCode = null;
    }

    public static function getAllTypes() {
        return array(
            'Visa',
            'Diners',
            'Mastercard',
            'Amex'
        );
    }

    public function setType($type) {
        $types = self::getAllTypes();

        if (in_array($type, $types)) {
            $this->type = $type;
            return true;
        }

        return false;
    }

    public function getType() {
        return $this->type;
    }

    public function setNumber($number) {
        $this->number = $number;
    }

    public function getNumber() {
        return $this->number;
    }

    public function getNotationNumber() {
        $number = $this->number;
        $numCount = strlen($number);
        $lastNum = substr($number, -4);
        return str_repeat('*', $numCount - 4).$lastNum;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getName() {
        return $this->name;
    }

    public function setExpiryMonth($expiryMonth) {
        if ($expiryMonth > 0 && $expiryMonth <= 12) {
            $this->expiryMonth = $expiryMonth;
            return true;
        }
        return false;
    }

    public function getExpiryMonth() {
        return $this->expiryMonth;
    }

    public function setExpiryYear($expiryYear) {
        $this->expiryYear = $expiryYear;
    }

    public function getExpiryYear() {
        return $this->expiryYear;
    }

    public function setSecurityCode($security_code) {
        $this->securityCode = $security_code;
    }

    public function getSecurityCode() {
        return $this->securityCode;
    }
}
