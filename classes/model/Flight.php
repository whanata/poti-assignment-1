<?php

require_once(__DIR__.'/../lib/SessionObject.php');
require_once(__DIR__.'/Seat.php');

class Flight extends SessionObject implements SessionObjectInterface {
    const SESSION_KEY = 'current_flight';

    private $id;
    private $routeNo;
    private $fromCity;
    private $toCity;
    private $price;
    private $seats;


	public function __construct($route_no = null, $from_city = null, $to_city = null, $price = null) {
        parent::__construct();
        $this->id = null;
        $this->routeNo = $route_no;
        $this->fromCity = $from_city;
        $this->toCity = $to_city;
        $this->price = $price;
        $this->seats = array();
	}

    public function copy($object) {
        self::__construct(
            $object->getRouteNo(),
            $object->getFromCity(),
            $object->getToCity(),
            $object->getPrice()
        );
        $this->id = $object->getId();
        $this->seats = $object->getSeats();
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getId() {
        return $this->id;
    }

    public function setRouteNo($route_no) {
        $this->routeNo = $route_no;
    }

    public function getRouteNo() {
        return $this->routeNo;   
    }

    public function setFromCity($from_city) {
        $this->fromCity = $from_city;
    }

    public function getFromCity() {
        return $this->fromCity;
    }

    public function setToCity($to_city) {
        $this->toCity = $to_city;
    }

    public function getToCity() {
        return $this->toCity;
    }

    public function setPrice($price) {
        $this->price = $price;
    }

    public function getPrice() {
        return $this->price;
    }

    public function addSeat($seat) {
        if (empty($this->seats)) {
            $this->seats = array();
        }
        array_push($this->seats, $seat);
    }

    public function removeSeats() {
        $this->seats = array();
    }

    public function setSeats($seats) {
        $this->seats = $seats;
    }

    public function getSeats() {
        return $this->seats;
    }

    public function numSeats() {
        return count($this->seats);
    }

    public function load() {
        $where_condition = array();

        if (!empty($this->routeNo)) {
            array_push($where_condition, sprintf("route_no = '%s'", $this->routeNo));
        }

        if (!empty($this->from_city)) {
            array_push($where_condition, sprintf("from_city = '%s'", $this->fromCity));
        }

        if (!empty($this->to_city)) {
            array_push($where_condition, sprintf("to_city = '%s'", $this->toCity));
        }

        if (!empty($this->price)) {
            array_push($where_condition, sprintf("price = '%s'", $this->price));
        }

        if (!empty($where_condition)) {
            $where_condition_string = implode(' AND ', $where_condition);
            $sql = sprintf('
                SELECT route_no, from_city, to_city, price
                FROM flights
                WHERE %s
            ', $where_condition_string);

            $db_result = DB::query($sql);
            $result = DB::getResult($db_result);
            
            foreach ($result as $row) {
                $this->setRouteNo($row['route_no']);
                $this->setFromCity($row['from_city']);
                $this->setToCity($row['to_city']);
                $this->setPrice($row['price']);
            }
            return true;
        }

        return false;
    }
}
