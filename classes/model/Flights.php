<?php

require_once(__DIR__.'/Flight.php');

class Flights {
    private $flights;
    private $latestId;

    public function __construct() {
        $this->flights = array();
        $this->latestId = 0;
    }

    public function addFlight($flight) {
        $flight_id = $this->latestId;
        $flight->setId($flight_id);
        array_push($this->flights, $flight);
        $this->latestId += 1;
        return $flight_id;
    }
    public function getFlight($flight_id) {
        return $this->flights[$flight_id];
    }

    public function getFlights() {
        return $this->flights;
    }

    public function deleteFlights($flight_ids) {
        foreach ($flight_ids as $id) {
            unset($this->flights[$id]);
        }
    }
}
