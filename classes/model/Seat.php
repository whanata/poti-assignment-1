<?php

class Seat {
    private $seatNo;
    private $child;
    private $wheelchair;
    private $specialDiet;

    public function __construct($seat_no = null, $child = false, $wheelchair = false, $special_diet = false) {
        $this->seatNo = $seat_no;
        $this->child = $child;
        $this->wheelchair = $wheelchair;
        $this->specialDiet = $special_diet;
    }

    public function setSeatNo($seat_no) {
        $this->seatNo = $seat_no;
    }

    public function getSeatNo() {
        return $this->seatNo;
    }

    public function setChild($child) {
        $this->child = $child;
    }

    public function getChild() {
        return $this->child;
    }

    public function setWheelchair($wheelchair) {
        $this->wheelchair = $wheelchair;
    }

    public function getWheelchair() {
        return $this->wheelchair;
    }

    public function setSpecialDiet($special_diet) {
        $this->specialDiet = $special_diet;
    }

    public function getSpecialDiet() {
        return $this->specialDiet;
    }
}
