<?php

require_once(__DIR__.'/../lib/SessionObject.php');
require_once(__DIR__.'/Flights.php');
require_once(__DIR__.'/CreditCard.php');

class User extends SessionObject implements SessionObjectInterface {
    const SESSION_KEY = 'user';

    private $givenName;
    private $familyName;
    private $emailAddress;
    private $address1;
    private $address2;
    private $suburb;
    private $state;
    private $country;
    private $postcode;
    private $mobilePhone;
    private $businessPhone;
    private $workPhone;
    private $flights;
    private $creditCard;

    public function __construct() {
        parent::__construct();
        $this->givenName = '';
        $this->familyAddress = '';
        $this->emailAddress = '';
        $this->address1 = '';
        $this->address2 = '';
        $this->suburb = '';
        $this->state = '';
        $this->country = '';
        $this->postcode = '';
        $this->mobilePhone = '';
        $this->businessPhone = '';
        $this->workPhone = '';
        $this->flights = new Flights();
        $this->creditCard = new CreditCard();
    }

    public function copy($object) {
        $this->givenName = $object->getGivenName();
        $this->familyName = $object->getFamilyName();
        $this->emailAddress = $object->getEmailAddress();
        $this->address1 = $object->getAddress1();
        $this->address2 = $object->getAddress2();
        $this->suburb = $object->getSuburb();
        $this->state = $object->getState();
        $this->country = $object->getCountry();
        $this->postcode = $object->getPostcode();
        $this->mobilePhone = $object->getMobilePhone();
        $this->businessPhone = $object->getBusinessPhone();
        $this->workPhone = $object->getWorkPhone();
        $this->flights = $object->getFlights();
        $this->creditCard = $object->getCreditCard();
    }

    public function setGivenName($given_name) {
        $this->givenName = $given_name;
    }

    public function getGivenName() {
        return $this->givenName;
    }

    public function setFamilyName($family_name) {
        $this->familyName = $family_name;
    }

    public function getFamilyName() {
        return $this->familyName;
    }

    public function setEmailAddress($email_address) {
        $this->emailAddress = $email_address;
    }

    public function getEmailAddress() {
        return $this->emailAddress;
    }

    public function setAddress1($address1) {
        $this->address1 = $address1;
    }

    public function getAddress1() {
        return $this->address1;
    }

    public function setAddress2($address2) {
        $this->address2 = $address2;
    }

    public function getAddress2() {
        return $this->address2;
    }

    public function setSuburb($suburb) {
        $this->suburb = $suburb;
    }

    public function getSuburb() {
        return $this->suburb;
    }

    public function setState($state) {
        $this->state = $state;
    }

    public function getState() {
        return $this->state;
    }

    public function setCountry($country) {
        $this->country = $country;
    }

    public function getCountry() {
        return $this->country;
    }

    public function setPostcode($postcode) {
        $this->postcode = $postcode;
    }

    public function getPostcode() {
        return $this->postcode;
    }

    public function setMobilePhone($mobile_phone) {
        $this->mobilePhone = $mobile_phone;
    }

    public function getMobilePhone() {
        return $this->mobilePhone;
    }

    public function setBusinessPhone($business_phone) {
        $this->businessPhone = $business_phone;
    }

    public function getBusinessPhone() {
        return $this->businessPhone;
    }

    public function setWorkPhone($work_phone) {
        $this->workPhone = $work_phone;
    }

    public function getWorkPhone() {
        return $this->workPhone;
    }

    public function addFlight($flight) {
        return $this->flights->addFlight($flight);
    }

    public function getFlight($flight_id) {
        return $this->flights->getFlight($flight_id);
    }

    public function getFlights() {
        return $this->flights;
    }

    public function deleteSelectedFlights($flight_ids) {
        $this->flights->deleteFlights($flight_ids);
    }

    public function deleteFlights() {
        $this->flights = new Flights();
    }

    public function setCreditCard($credit_card) {
        $this->creditCard = $credit_card;
    }

    public function getCreditCard() {
        return $this->creditCard;
    }

}
