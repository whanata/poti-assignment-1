<?php

require_once(__DIR__.'/Page.php');
require_once(__DIR__.'/../model/Flight.php');

class ChooseSeatPage extends Page implements PageInterface {
    private $flight;

    public function __construct() {
        parent::__construct('choose-seat');
        $this->flight = new Flight();

        if ($this->flight->getFromSession() === false) {
            render404();
        }

        $this->vars['title'] = 'Choose Seats';
    }

    public function getFlightInfo() {
        $flight = $this->flight;
        $this->vars['from_city'] = $flight->getFromCity();
        $this->vars['to_city'] = $flight->getToCity();
        $this->vars['price'] = $flight->getPrice();
    }

    public function getTable() {
        $html_string = '';
        for($seat_no = 0; $seat_no < 5; $seat_no++) {
            $html_string .= '<tr>';
            $html_string .= sprintf('<td>%s</td>', $seat_no);
            $html_string .= sprintf('<td>
                <input class="seat-feature%s" onclick="checkSeatFeatures(%s)" type="checkbox" name="child[]" value="%s" />
            </td>', $seat_no, $seat_no, $seat_no);
            $html_string .= sprintf('<td>
                <input class="seat-feature%s" onclick="checkSeatFeatures(%s)" type="checkbox" name="wheelchair[]" value="%s" />
            </td>', $seat_no, $seat_no, $seat_no);
            $html_string .= sprintf('<td>
                <input class="seat-feature%s" onclick="checkSeatFeatures(%s)" type="checkbox" name="special_diet[]" value="%s" />
            </td>', $seat_no, $seat_no, $seat_no);
            $html_string .= sprintf('<td>
                <input id="selected-seat%s" class="select-seats" onclick="checkSelectSeat(%s)" type="checkbox" name="seats[]" value="%s" />
            </td>', $seat_no, $seat_no, $seat_no);
            $html_string .= '</tr>';
        }
        $this->vars['seat_table'] = $html_string;
    }

    public function render() {
        $this->getFlightInfo();
        $this->getTable();
        parent::render();
    }
}
