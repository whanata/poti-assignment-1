<?php

require_once(__DIR__.'/Page.php');
require_once(__DIR__.'/../model/Flight.php');
require_once(__DIR__.'/../model/User.php');

class ConfirmBookingPage extends Page implements PageInterface {
    private $flight;
    private $user;

    public function __construct() {
        parent::__construct('confirm-booking');
        
        $this->flight = new Flight();
        if ($this->flight->getFromSession() === false) {
            render404();
        }

        $this->user = new User();
        if ($this->user->getFromSession() === false) {
            render404();
        }

        $this->vars['title'] = 'Stage 3 - Review Bookings and Details';
    }

    public function getFlightInfo() {
        $flight = $this->flight;
        $this->vars['from_city'] = $flight->getFromCity();
        $this->vars['to_city'] = $flight->getToCity();
        $this->vars['price'] = $flight->getPrice();
    }

    public function getBookedSeatTable() {
        $html_string = '';
        $seats = $this->flight->getSeats();
        foreach ($seats as $seat) {
            $html_string .= '<tr>';
            $html_string .= sprintf('<td>%s</td>', $seat->getSeatNo());

            if ($seat->getChild()) {
                $html_string .= '<td class="success"></td>';
            } else {
                $html_string .= '<td></td>';
            }

            if ($seat->getWheelchair()) {
                $html_string .= '<td class="success"></td>';
            } else {
                $html_string .= '<td></td>';
            }

            if ($seat->getSpecialDiet()) {
                $html_string .= '<td class="success"></td>';
            } else {
                $html_string .= '<td></td>';
            }

            $html_string .= '</tr>';
        }
        $this->vars['booked_seat_table'] = $html_string;
    }

    public function getUserDetails() {
        $user = $this->user;
        $this->vars['name'] = sprintf('Name: %s %s', $user->getGivenName(), $user->getFamilyName());
        $this->vars['email'] = sprintf('Email Address: %s', $user->getEmailAddress());
        $this->vars['address1'] = sprintf('Address Line 1: %s', $user->getAddress1());
        $this->vars['address2'] = sprintf('Address Line 2: %s', $user->getAddress2());
        $this->vars['suburb'] = sprintf('Suburb: %s', $user->getSuburb());
        $this->vars['state'] = sprintf('State: %s', $user->getState());
        $this->vars['country'] = sprintf('Country: %s', $user->getCountry());
        $this->vars['postcode'] = sprintf('Postcode: %s', $user->getPostcode());
        $this->vars['mobile_phone'] = sprintf('Mobile Phone: %s', $user->getMobilePhone());
        $this->vars['business_phone'] = sprintf('Business Phone: %s', $user->getBusinessPhone());
        $this->vars['work_phone'] = sprintf('Work Phone: %s', $user->getWorkPhone());
    }

    public function getNotationCreditCardDetails() {
        $creditCard = $this->user->getCreditCard();
        $this->vars['notation_credit_card_number'] = sprintf('Credit Card Number: %s', $creditCard->getNotationNumber());
        $this->vars['credit_card_type'] = sprintf('Credit Card Type: %s', $creditCard->getType());
    }

    public function render() {
        $this->getFlightInfo();
        $this->getBookedSeatTable();
        $this->getUserDetails();
        $this->getNotationCreditCardDetails();
        parent::render();
    }

}
