<?php

require_once(__DIR__.'/Page.php');

class ContactUsPage extends Page implements PageInterface {
    private $user;

    public function __construct() {
        parent::__construct('contact-us');

        $this->user = new User();
        $this->user->getFromSession();

        $session = new Session();
        $fields = $session->getSession('fields', true, array());
        $this->getFields($fields);

        $this->vars['title'] = 'Contact Us';
    }

    public function getFields($fields) {
        if (!empty($fields)) {
            $this->vars = array_merge($this->vars, $fields);
        } else {
            $this->vars['first_name'] = $this->user->getGivenName();
            $this->vars['last_name'] = $this->user->getFamilyName();
            $this->vars['email_address'] = $this->user->getEmailAddress();
        }
    }

    public function render() {
        parent::render();
    }
}
