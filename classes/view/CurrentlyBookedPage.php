<?php

require_once(__DIR__.'/Page.php');
require_once(__DIR__.'/../model/Flight.php');
require_once(__DIR__.'/../model/User.php');

class CurrentlyBookedPage extends Page implements PageInterface {
    private $flight;
    private $user;

    public function __construct() {
        parent::__construct('currently-booked');
        $this->flight = new Flight();

        if ($this->flight->getFromSession() === false) {
            render404();
        }

        $this->user = new User();
        if ($this->user->getFromSession() === false) {
            render404();
        }

        $this->vars['title'] = 'Current Booking';
    }

    public function getFlightInfo() {
        $flight = $this->flight;
        $this->vars['from_city'] = $flight->getFromCity();
        $this->vars['to_city'] = $flight->getToCity();
        $this->vars['price'] = $flight->getPrice();
    }

    public function getBookedSeatTable() {
        $html_string = '';
        $seats = $this->flight->getSeats();
        foreach ($seats as $seat) {
            $html_string .= '<tr>';
            $html_string .= sprintf('<td>%s</td>', $seat->getSeatNo());

            if ($seat->getChild()) {
                $html_string .= '<td class="success"></td>';
            } else {
                $html_string .= '<td></td>';
            }

            if ($seat->getWheelchair()) {
                $html_string .= '<td class="success"></td>';
            } else {
                $html_string .= '<td></td>';
            }

            if ($seat->getSpecialDiet()) {
                $html_string .= '<td class="success"></td>';
            } else {
                $html_string .= '<td></td>';
            }

            $html_string .= '</tr>';
        }
        $this->vars['booked_seat_table'] = $html_string;
    }

    public function getYourBookingTable() {
        $flight_id = $this->flight->getId();
        $flights = $this->user->getFlights()->getFlights();
        unset($flights[$flight_id]);
        if (!empty($flights)) {
            $row_string = '';
            foreach ($flights as $flight) {
                $row_string .= '<tr>';
                $row_string .= sprintf('<td>%s</td>', $flight->getFromCity());
                $row_string .= sprintf('<td>%s</td>', $flight->getToCity());
                $row_string .= sprintf('<td>%s</td>', $flight->getPrice());
                $row_string .= sprintf('<td>%s</td>', $flight->numSeats());
                $row_string .= '</tr>';
            }

            $html_string = sprintf('
                <div class="text-center">
                    <h2>Other Bookings</h2>
                </div>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>From City</th>
                            <th>To City</th>
                            <th>Price (USD)</th>
                            <th>Seats Selected</th>
                        </tr>
                    </thead>
                    <tbody>
                        %s
                    </tbody>
                </table>
            ', $row_string);
        } else {
            $html_string = '
                <div class="text-center">
                    <h2>No Other Bookings</h2>
                </div>
            ';
        }
        $html_string .= '
            <div class="row">
                <div class="col-md-4">
                    <a href="index.php?page=search-flight" class="btn btn-default text-center">Book More Flights</a>
                </div>
                <div class="col-md-4 text-center">
                    <a href="index.php?page=currently-booked&action=clearFlight" class="btn btn-default text-center">Clear All Booked Flights</a>
                </div>
                <div class="col-md-4">
                    <a href="index.php?page=currently-booked&action=submit" class="btn btn-primary text-center pull-right">Proceed to Checkout</a>
                </div>
            </div>
        ';

        $this->vars['your_booking_table'] = $html_string;
    }

    public function render() {
        $this->getFlightInfo();
        $this->getBookedSeatTable();
        $this->getYourBookingTable();
        parent::render();
    }
}
