<?php

require_once(__DIR__.'/Page.php');

class DonePage extends Page implements PageInterface {
    private $user;

    public function __construct() {
        parent::__construct('done');

        $this->user = new User();
        if ($this->user->getFromSession() === false) {
            render404();
        }

        $this->vars['title'] = 'Done';
    }

    public function getUserDetails() {
        $this->vars['given_name'] = $this->user->getGivenName();
        $this->vars['family_name'] = $this->user->getFamilyName();
        $this->vars['email'] = $this->user->getEmailAddress();
    }

    public function render() {
        $this->getUserDetails();
        parent::render();
        session_start();
        session_destroy();
    }
}
