<?php

require_once(__DIR__.'/Page.php');

class HomePage extends Page {

    public function __construct() {
        parent::__construct('home');
        $this->vars['title'] = 'Home Page';
    }
}
