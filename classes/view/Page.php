<?php

require_once(__DIR__.'/../../util/util.php');
require_once(__DIR__.'/PageInterface.php');

class Page implements PageInterface {
    protected $vars;
    protected $pageName;
    protected $error;
    protected $session;

    public function __construct($pageName) {
        $this->vars = array();
        $this->pageName = $pageName;
        $this->error = new Error();
        $this->session = new Session();

        $this->vars['bootstrap_css_filepath'] = 'http://www-student.it.uts.edu.au/~wtjo/poti-1/static/css/bootstrap.min.css';
        $this->vars['general_css_filepath'] = 'http://www-student.it.uts.edu.au/~wtjo/poti-1/static/css/general.css';
    }

    public function render() {
        $vars = $this->vars;
        $this->error->getFromSession(true);

        echo '
            <!DOCTYPE html>
            <html>
        ';
        require_once(__DIR__.'/../../static/templates/head.html');
        echo '<body>';
        require_once(__DIR__.'/../../static/templates/nav.html');
        require_once(__DIR__.'/../../static/templates/error.html');
        require_once(__DIR__.'/../../static/templates/'.$this->pageName.'.html');
        echo '</body>';
        echo '</html>';
    }
}
