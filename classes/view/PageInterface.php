<?php

interface PageInterface {

    public function render();
}
