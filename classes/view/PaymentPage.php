<?php

require_once(__DIR__.'/Page.php');
require_once(__DIR__.'/../model/Flight.php');
require_once(__DIR__.'/../model/User.php');
require_once(__DIR__.'/../model/CreditCard.php');

class PaymentPage extends Page implements PageInterface {
    private $flight;
    private $user;

    public function __construct() {
        parent::__construct('payment');
        
        $this->flight = new Flight();
        if ($this->flight->getFromSession() === false) {
            render404();
        }

        $this->user = new User();
        if ($this->user->getFromSession() === false) {
            render404();
        }

        $this->vars['title'] = 'Complete Booking - Stage 2 of 4 - Payment Details';
    }

    public function getUserDetails() {
        $user = $this->user;
        $this->vars['name'] = sprintf('Name: %s %s', $user->getGivenName(), $user->getFamilyName());
        $this->vars['email'] = sprintf('Email Address: %s', $user->getEmailAddress());
        $this->vars['address1'] = sprintf('Address Line 1: %s', $user->getAddress1());
        $this->vars['address2'] = sprintf('Address Line 2: %s', $user->getAddress2());
        $this->vars['suburb'] = sprintf('Suburb: %s', $user->getSuburb());
        $this->vars['state'] = sprintf('State: %s', $user->getState());
        $this->vars['country'] = sprintf('Country: %s', $user->getCountry());
        $this->vars['postcode'] = sprintf('Postcode: %s', $user->getPostcode());
        $this->vars['mobile_phone'] = sprintf('Mobile Phone: %s', $user->getMobilePhone());
        $this->vars['business_phone'] = sprintf('Business Phone: %s', $user->getBusinessPhone());
        $this->vars['work_phone'] = sprintf('Work Phone: %s', $user->getWorkPhone());
    }

    public function createOptions($list, $current_element='') {
        $option_string = '<option></option>';
        foreach ($list as $element) {
            if ($element === $current_element) {
                $option_string .= '<option selected>'.$element.'</option>';
            } else {
                $option_string .= '<option>'.$element.'</option>';
            }
        }
        return $option_string;
    }

    public function creditCardTypeDropdown() {
        $types = CreditCard::getAllTypes();
        $type_options = $this->createOptions($types);
        $this->vars['credit_card_type_options'] = $type_options;
    }

    public function expiryMonthDropdown() {
        $months = range(1, 12);
        $month_count = count($months);
        $months = array_map('str_pad', $months, array_fill(0, $month_count, 2), array_fill(0, $month_count, 0), array_fill(0, $month_count, STR_PAD_LEFT));
        $month_options = $this->createOptions($months);
        $this->vars['month_options'] = $month_options;
    }

    public function expiryYearDropdown() {
        $current_year = date('y');
        $years = range($current_year, 99);
        $year_count = count($years);
        $years = array_map('str_pad', $years, array_fill(0, $year_count, 2), array_fill(0, $year_count, 0), array_fill(0, $year_count, STR_PAD_LEFT));
        $year_options = $this->createOptions($years);
        $this->vars['year_options'] = $year_options;
    }

    public function render() {
        $this->getUserDetails();
        $this->creditCardTypeDropdown();
        $this->expiryMonthDropdown();
        $this->expiryYearDropdown();
        parent::render();
    }
}
