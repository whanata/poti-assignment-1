<?php

require_once(__DIR__.'/Page.php');
require_once(__DIR__.'/../model/Flight.php');
require_once(__DIR__.'/../model/User.php');

class PersonalDetailPage extends Page implements PageInterface {
    private $flight;
    private $user;

    public function __construct() {
        parent::__construct('personal-detail');

        $this->flight = new Flight();
        if ($this->flight->getFromSession() === false) {
            render404();
        }

        $this->user = new User();
        if ($this->user->getFromSession() === false) {
            render404();
        }

        $session = new Session();
        $fields = $session->getSession('fields', true, array());
        $this->getFields($fields);

        $this->vars['title'] = 'Complete Booking - Stage 1 of 4 - Personal Details';
    }

    public function createOptions($list, $current_element='') {
        $option_string = '<option></option>';
        foreach ($list as $element) {
            if ($element === $current_element) {
                $option_string .= '<option selected>'.$element.'</option>';
            } else {
                $option_string .= '<option>'.$element.'</option>';
            }
        }
        return $option_string;
    }

    public function getFields($fields) {
        if (!empty($fields)) {
            $this->vars = array_merge($this->vars, $fields);
        } else {
            $this->vars['given_name'] = $this->user->getGivenName();
            $this->vars['family_name'] = $this->user->getFamilyName();
            $this->vars['email'] = $this->user->getEmailAddress();
            $this->vars['address1'] = $this->user->getAddress1();
            $this->vars['address2'] = $this->user->getAddress2();
            $this->vars['suburb'] = $this->user->getSuburb();
            $this->vars['state'] = $this->user->getState();
            $this->vars['country'] = $this->user->getCountry();
            $this->vars['postcode'] = $this->user->getPostcode();
            $this->vars['mobile_phone'] = $this->user->getMobilePhone();
            $this->vars['business_phone'] = $this->user->getBusinessPhone();
            $this->vars['work_phone'] = $this->user->getWorkPhone();

        }
    }

    public function stateInput() {
        $country = get($this->vars, 'country');
        $state = get($this->vars, 'state');

        $states = array(
            'ACT',
            'NSW',
            'NT',
            'QLD',
            'SA',
            'TAS',
            'VIC',
            'WA'
        );

        if ($country === 'Australia') {
            $state_options = $this->createOptions($states, $state);
            $state_input = sprintf('
                <select class="form-control" id="state" name="state">
                    %s
                </select>
            ', $state_options);
        } else {
            $state_input = sprintf('
                <input type="text" name="state" class="form-control" value="%s" id="state" />
            ', $state);
        }

        $this->vars['state_input'] = $state_input;
    }

    public function countryDropdown() {
        $countries = getCountries();
        $country_options = $this->createOptions($countries, get($this->vars, 'country'));
        $this->vars['country_options'] = $country_options;
    }

    public function render() {
        $this->countryDropdown();
        $this->stateInput();
        parent::render();
    }
}
