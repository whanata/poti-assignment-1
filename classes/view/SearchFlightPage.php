<?php

require_once(__DIR__.'/Page.php');

class SearchFlightPage extends Page implements PageInterface {

    public function __construct() {
        parent::__construct('search-flight');
        $this->vars['title'] = 'Search Flights';
    }

    public function searchFlights($from_to_cities) {
        $city_sqls = array();
        $from_city = get($from_to_cities, 0);
        $to_city = get($from_to_cities, 1);

        if (!empty($from_city)) {
            array_push($city_sqls, sprintf("from_city = '%s'", $from_city));
        }

        if (!empty($to_city)) {
            array_push($city_sqls, sprintf("to_city = '%s'", $to_city));
        }

        $city_sql_string = implode(' AND ', $city_sqls);

        $sql = sprintf('
            SELECT route_no, from_city, to_city, price
            FROM flights
            WHERE %s
        ', $city_sql_string);

        $db_result = DB::query($sql);
        $result = DB::getResult($db_result);

        return $result;
    }

    public function getCities($option) {
        $cities = array();

        $from_to_city = $option.'_city';

        $sql = sprintf('
            SELECT DISTINCT %s
            FROM flights
            ORDER BY 1 ASC
        ', $from_to_city);

        $db_result = DB::query($sql);

        $result = DB::getResult($db_result);

        foreach ($result as $row) {
            array_push($cities, $row[$from_to_city]);
        }

        return $cities;
    }

    public function createOptions($list, $current_element='') {
        $option_string = '<option></option>';
        foreach ($list as $element) {
            if ($element === $current_element) {
                $option_string .= '<option selected>'.$element.'</option>';
            } else {
                $option_string .= '<option>'.$element.'</option>';
            }
        }
        return $option_string;
    }

    // Check if flight search has been used
    public function flightSearchSelected() {
        if (array_key_exists('to_city', $_POST) || array_key_exists('from_city', $_POST)) {
            if (!empty($_POST['to_city']) || !empty($_POST['from_city'])) {
                return true;
            }
        }
        return false;
    }

    public function getCityOptions() {
        $from_cities = $this->getCities('from');
        $to_cities = $this->getCities('to');
        $from_selected_city = '';
        $to_selected_city = '';
        if ($this->flightSearchSelected()) {
            $from_selected_city = get($_POST, 'from_city');
            $to_selected_city = get($_POST, 'to_city');
        }
        $this->vars['from_cities'] = $this->createOptions($from_cities, $from_selected_city);
        $this->vars['to_cities'] = $this->createOptions($to_cities, $to_selected_city);
    }

    public function getTable() {
        if ($this->flightSearchSelected()) {
            $row_string = '';
            $from_to_cities = array($_POST['from_city'], $_POST['to_city']);
            $flights = $this->searchFlights($from_to_cities);
            if (!empty($flights)) {
                foreach ($flights as $flight) {
                    $row_string .= '<tr>';
                    $row_string .= sprintf('<td>%s</td>', $flight['from_city']);
                    $row_string .= sprintf('<td>%s</td>', $flight['to_city']);
                    $row_string .= sprintf('<td>$%s</td>', $flight['price']);
                    $row_string .= sprintf('<td><input type="radio" name="selected_flight" value="%s" /></td>', $flight['route_no']);
                    $row_string .= '</tr>';
                }
                $html_string = sprintf('
                    <form method="post" action="index.php?page=search-flight&action=submit">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th>From City</th>
                                    <th>To City</th>
                                    <th>Price (USD)</th>
                                    <th>Select</th>
                                </tr>
                            </thead>
                            <tbody>
                                %s
                            </tbody>
                        </table>
                        <div>
                            <button type="submit" class="btn btn-primary pull-right">Make Booking for Selected Flight</button>
                        </div>
                    </form>
                ', $row_string);
            } else {
                // No Matches
                $html_string = '
                    <div class="text-center">
                        <h3>No Result</h3>
                    </div>
                ';
            }
        } else {
            $html_string = '';
        }
        $this->vars['flight_table'] = $html_string;
    }

    public function render() {
        $this->getCityOptions();
        $this->getTable();
        parent::render();
    }
}
