<?php

require_once(__DIR__.'/Page.php');
require_once(__DIR__.'/../model/User.php');

class YourBookingPage extends Page implements PageInterface {
    private $user;

    public function __construct() {
        parent::__construct('your-booking');

        $this->user = new User();
        $this->user->getFromSession();

        $this->vars['title'] = 'Your Bookings';
    }

    public function getYourBookingTable() {
        $flights = $this->user->getFlights()->getFlights();
        if (!empty($flights)) {
            $row_string = '';
            foreach ($flights as $flight) {
                $row_string .= '<tr>';
                $row_string .= sprintf('<td>%s</td>', $flight->getFromCity());
                $row_string .= sprintf('<td>%s</td>', $flight->getToCity());
                $row_string .= sprintf('<td>%s</td>', $flight->getPrice());
                $row_string .= sprintf('<td>%s</td>', $flight->numSeats());
                $row_string .= sprintf('<td><input name="selected_flight[]" value="%s" type="checkbox"></input></td>', $flight->getId());
                $row_string .= '</tr>';
            }

            $html_string = sprintf('
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>From City</th>
                            <th>To City</th>
                            <th>Price (USD)</th>
                            <th>Seats Selected</th>
                            <th>Selected</th>
                        </tr>
                    </thead>
                    <tbody>
                        %s
                    </tbody>
                </table>
            ', $row_string);
        $html_string .= '
            <div class="row">
                <div class="col-md-6">
                    <button name="delete_flight" value="1" class="btn btn-default">Delete Selected Flights</button>
                </div>
                <div class="col-md-6">
                    <button name="checkout" value="1" class="btn btn-primary pull-right">Proceed to Checkout</button>
                </div>
            </div>
        ';
        } else {
            $html_string = '
                <div class="text-center">
                    <h2>You have No Bookings</h2>
                </div>
            ';
        }

        $this->vars['your_booking_table'] = $html_string;
    }

    public function render() {
        $this->getYourBookingTable();
        parent::render();
    }
}
