<?php

require_once(__DIR__.'/classes/view/HomePage.php');
require_once(__DIR__.'/classes/view/SearchFlightPage.php');
require_once(__DIR__.'/classes/view/ChooseSeatPage.php');
require_once(__DIR__.'/classes/view/CurrentlyBookedPage.php');
require_once(__DIR__.'/classes/view/PersonalDetailPage.php');
require_once(__DIR__.'/classes/view/PaymentPage.php');
require_once(__DIR__.'/classes/view/ConfirmBookingPage.php');
require_once(__DIR__.'/classes/view/DonePage.php');
require_once(__DIR__.'/classes/view/ContactUsPage.php');
require_once(__DIR__.'/classes/view/YourBookingPage.php');

require_once(__DIR__.'/classes/controller/SearchFlightPageController.php');
require_once(__DIR__.'/classes/controller/ChooseSeatPageController.php');
require_once(__DIR__.'/classes/controller/CurrentlyBookedPageController.php');
require_once(__DIR__.'/classes/controller/PersonalDetailPageController.php');
require_once(__DIR__.'/classes/controller/PaymentPageController.php');
require_once(__DIR__.'/classes/controller/ConfirmBookingPageController.php');
require_once(__DIR__.'/classes/controller/ContactUsPageController.php');
require_once(__DIR__.'/classes/controller/YourBookingPageController.php');

require_once(__DIR__.'/util/util.php');

function getPage() {
    $pages = array(
        'search-flight' => 'SearchFlightPage',
        'choose-seat' => 'ChooseSeatPage',
        'currently-booked' => 'CurrentlyBookedPage',
        'contact-us' => 'ContactUsPage',
        'personal-detail' => 'PersonalDetailPage',
        'payment' => 'PaymentPage',
        'confirm-booking' => 'ConfirmBookingPage',
        'done' => 'DonePage',
        'your-booking' => 'YourBookingPage',
    );

    if (!array_key_exists('page', $_GET)) {
        $home_page = new HomePage();
        $home_page->render();
    } elseif (in_array($_GET['page'], array_keys($pages))) {
        if (array_key_exists('action', $_GET)) {
            $page_controller_class = $pages[$_GET['page']].'Controller';
            $page_controller = new $page_controller_class;
            $page_controller->{$_GET['action']}();
        } else {
            $page = new $pages[$_GET['page']];
            $page->render();
        }
    } else {
        render404();
    }
}

getPage();
