function updateSeatSelected() {
    var seat_count = 0;
    var seats = document.querySelectorAll('input[class="select-seats"]');
    for (var i = 0; i < seats.length; i++) {
        if (seats[i].checked) {
            seat_count++;
        }
    }
    console.log(seat_count);

    document.querySelector('div[id="seat-count"]').textContent = seat_count + ' Number of Seats Selected';
}

function checkSeatFeatures(seat_no) {
    var seat_features = document.querySelectorAll('input[class="seat-feature' + seat_no + '"]');
    for (var i = 0; i < seat_features.length; i++) {
        if (seat_features[i].checked) {
            document.querySelector('input[id="selected-seat' + seat_no + '"]').checked = true;
        }
    }
    updateSeatSelected();

}

function checkSelectSeat(seat_no) {
    var select_seat = document.querySelector('input[id="selected-seat' + seat_no + '"]');
    if (!select_seat.checked) {
        var seat_features = document.querySelectorAll('input[class="seat-feature' + seat_no + '"]');
        for (var i = 0; i < seat_features.length; i++) {
            seat_features[i].checked = false;
        }
    }
    updateSeatSelected();
}
