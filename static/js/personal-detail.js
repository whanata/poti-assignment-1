function updateRequired() {
    var states = [
        'ACT',
        'NSW',
        'NT',
        'QLD',
        'SA',
        'TAS',
        'VIC',
        'WA'
    ];
    var state_element = document.querySelector('#state');

    var selected = document.querySelector('#country').selectedOptions[0].value;
    if (selected === 'Australia') {
        state_element.parentElement.classList.add('required');
        document.querySelector('#postcode').parentElement.classList.add('required');
        var select = document.createElement('select');
        select.setAttribute('class', 'form-control');
        select.setAttribute('id', 'state');
        select.setAttribute('name', 'state');
        var option = document.createElement('option');
        option.text = '';
        select.appendChild(option);
        for (var i = 0; i < states.length; i++) {
            var option = document.createElement('option');
            option.text = states[i];
            select.appendChild(option);
        }
        state_element.replaceWith(select);

    } else {
        state_element.parentElement.classList.remove('required');
        document.querySelector('#postcode').parentElement.classList.remove('required');
        var input = document.createElement('input');
        input.setAttribute('class', 'form-control');
        input.setAttribute('id', 'state');
        input.setAttribute('name', 'state');
        input.setAttribute('type', 'text');
        state_element.replaceWith(input);
    }
}
